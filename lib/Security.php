<?php
/**
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
**/
class Security {

	/**
	* Usada pra guardar as configurações de segurança
	* @var array
	*/
	private $ar_settings;


	/**
	* Contruturo seta as configurações de segurança
	*
	* @param void
	* @return void
	*/
	public function __construct() {
		$this->ar_settings['openSession'] = true;
		$this->ar_settings['alwaysValidate'] = true;
		$this->startSession();
	}


	/**
	* Instância uma sessão
	*
	* @param void
	* @return void
	*/
	private function startSession() {
		if ($this->ar_settings['openSession'] == true) {
			if ($this->isSessionStarted() === false) {
				session_start();
			}
		}
	}


	/**
	* Destrói a sessão
	*
	* @param void
	* @return void
	*/
	public function logout() {
		if ($this->isSessionStarted() === true) {
			session_destroy();
		}
	}


	/**
	* Função que valida um usuário e senha
	*
	* @param object $o_login
	* @return void
	*/
	public function setSessionParameters($o_login) {
		$_SESSION['memberID'] = $o_login->getId();
		$_SESSION['memberName'] = $o_login->getName();

		if ($this->ar_settings['alwaysValidate'] == true) {
			$_SESSION['memberLogin'] = $o_login->getLogin();
			$_SESSION['memberPassword'] = $o_login->getPassword();
			$_SESSION['memberPermission'] = $o_login->getPermissionId();
		}
	}

	/**
	* Função que protege uma página
	*
	* @param int $permissionLevel
	* @return void
	*/
	public function pagePermission($permissionLevel) {
		
		$totalAccess = 0;

		if ($totalAccess != $permissionLevel) {
			if ($this->isLogged()) {
				Application::redirect('?controle=Login&acao=showLogin');
			} else {
				if ($_SESSION['memberPermission'] < $permissionLevel) {
					Application::redirect('?controle=Home&acao=showAdministration');
				}
			}
		}
	}

	/**
	* Função que protege uma página
	*
	* @param void
	* @return boolean
	*/
	private function isLogged() {
		if (!isset($_SESSION['memberID']) OR !isset($_SESSION['memberName'])) {
			return false;
		} else if (!isset($_SESSION['memberID']) OR !isset($_SESSION['memberName'])) {
			if ($this->settings['alwaysValidate'] == true) {
				if (!validateUser($_SESSION['memberLogin'], $_SESSION['memberPassword'])) {
					return false;
				}
			}
			return true;
		}
	}

	/**
	* Retorna se sessão existe
	*
	* @param void
	* @return boolean
	*/
	private function isSessionStarted() {
		if (php_sapi_name() !== 'cli') {
			if (version_compare(phpversion(), '5.4.0', '>=')) {
				return session_status() === PHP_SESSION_ACTIVE ? true : false;
			} else {
				return session_id() === '' ? false : true;
			}
		}
		return false;
	}
}

?>