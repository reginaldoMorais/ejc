<?php
/**
* Configuração de Bando de Dados
*
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
abstract class PersistModelAbstract {
	/**
	* Variável responsável por guardar dados da conexão do banco
	* @var resource
	*/
	protected $o_db;
	protected $st_banco;
	
	function __construct() {
		//Conectando ao banco de dados
		$st_host = 'localhost';
		$st_port = 3306;
		$st_banco = 'ejc';
		$st_usuario = 'root';
		$st_senha = '';

		$st_dsn = "mysql:host=$st_host;port=$st_port;dbname=$st_banco";
		$this->st_banco = $st_banco;
		$this->o_db = new PDO($st_dsn, $st_usuario, $st_senha);
		$this->o_db->setAttribute ( PDO::ATTR_ERRMODE , PDO::ATTR_PERSISTENT, PDO::ERRMODE_EXCEPTION );
	}
}
?>