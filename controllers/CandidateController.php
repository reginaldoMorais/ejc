<?php
/**
* Incluindo classes externas requeridas
*/
//require_once 'models/HomeModel.php';


/**
* Responsável por gerenciar o fluxo de dados entre a camada de modelo e a de visualização
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class CandidateController {
	/**
	* Exibe a página de informações sobre inscrições
	* 
	* @param void
	* @return void
	*/
	public function requestInfoAction() {
		$o_view = new View('views/requestInfo.phtml');
		$o_view->showContents();
	}


	/**
	* Exibe a página com formulário de inscrição
	* 
	* @param void
	* @return void
	*/
	public function requestFormAction() {
		$o_view = new View('views/requestForm.phtml');
		$o_view->showContents();
	}
}
?>