<?php
/**
* Incluindo classes externas requeridas
*/
require_once 'models/LoginModel.php';


/**
* Responsável por gerenciar o fluxo de dados entre a camada de modelo e a de visualização
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
 */
class LoginController {
	/**
	* Exibe a página de login
	* 
	* @param void
	* @return void
	*/
	public function showLoginAction() {
		$o_view = new View('views/login.phtml');
		$o_view->showContents();
	}


	/**
	* Efetua login
	* 
	* @param void
	* @return void
	*/
	public function loginAction() {
		if (count($_POST) > 0) {
			if (!empty($_POST['login']) AND !empty($_POST['password'])) {
				$o_login = new LoginModel();
				$o_login->setLogin($_POST['login']);
				$o_login->setPassword($_POST['password']);
				if ($o_login->validateUser() === true)
					Application::redirect('?controle=Home&acao=showAdministration');
			}
		}
			
		$o_view = new View('views/login.phtml');
		$o_view->setParams(array('st_errorMessage' => 'Login ou senha inválidos'));
		$o_view->showContents();
	}


	/**
	* Efetua logout
	* 
	* @param void
	* @return void
	*/
	public function logoutAction() {
		$o_Login = new LoginModel();
		$o_Login->logout();
		Application::redirect('?controle=Home&acao=showHome');
	}
}
?>