<?php
/**
* Incluindo classes externas requeridas
*/
require_once 'models/MemberModel.php';
require_once 'models/ContactModel.php';
require_once 'models/ImageModel.php';
require_once 'models/MemberEJCModel.php';
require_once 'models/MemberCFLModel.php';

/**
* Responsável por gerenciar o fluxo de dados entre a camada de modelo e a de visualização
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
 */
class MemberController {
	/**
	* Exibe a página com os dados do membro
	* 
	* @param void
	* @return void
	*/
	public function showAction() {
		if (!empty($_GET['id'])) {
			$o_member = new MemberModel();
			$o_member->setId($_GET['id']);
			
			if ($o_member->getById() === true) {
				$o_contact = new ContactModel();
				$o_image = new ImageModel();
				$o_memberEJC = new MemberEJCModel();
				//$o_memberCFL = new MemberCFLModel();

				// Recupera o contato do membro
				$o_contact->setId = $o_member->getContactId();
				$o_contact->getById();
				
				// Recupera as images do membro
				$o_image->getAllByMemberId($o_member->getId());
				
				// Recupera as participações do membro
				$o_memberEJC->getAllByMemberId($o_member->getId());

				// Recupera as participações do membro no CFL
				//$o_memberCFL->getAllByMemberId($o_member->getId);

				$o_member->setContact($o_contact);
				$o_member->setImage($o_image);
				$o_member->setMemberEJC($o_memberEJC);
				//$o_member->setMemberCFL($o_memberCFL);
				
				$o_view = new View('views/member.phtml');
				$o_view->setParams(array('o_member' => $o_member));
				$o_view->showContents();
			}
		}
		$o_view = new View('views/404.phtml');
		$o_view->setParams(array('st_errorMessage' => 'Membro não foi encontrado'));
		$o_view->showContents();
	}


	/**
	* Exibe a página de lista de membros
	* 
	* @param void
	* @return void
	*/
	public function listAction() {
		$o_member = new MemberModel();
		$o_member->getList();

		$o_view = new View('views/memberList.phtml');
		$o_view->setParams(array('o_member' => $o_member));
		$o_view->showContents();
	}
}
?>