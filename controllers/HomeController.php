<?php
/**
* Incluindo classes externas requeridas
*/
//require_once 'models/HomeModel.php';


/**
* Responsável por gerenciar o fluxo de dados entre a camada de modelo e a de visualização
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class HomeController {
	/**
	* Exibe a página de home
	* 
	* @param void
	* @return void
	*/
	public function showHomeAction() {
		$o_view = new View('views/home.phtml');
		$o_view->showContents();
	}


	/**
	* Exibe a página administrativa
	* 
	* @param void
	* @return void
	*/
	public function showAdministrationAction() {
		$o_view = new View('views/administration.phtml');
		$o_view->showContents();
	}
}
?>