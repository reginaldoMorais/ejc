<?php
/**
* Incluindo classes externas requeridas
*/
require_once 'models/EJCModel.php';

/**
* Responsável por gerenciar o fluxo de dados entre a camada de modelo e a de visualização
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
 */
class EJCController {
	/**
	* Exibe a página com os dados do EJC
	* 
	* @param void
	* @return void
	*/
	public function showAction() {
		$o_view = new View('views/ejc.phtml');
		$o_view->showContents();
	}


	/**
	* Exibe a página de lista de EJCs
	* 
	* @param void
	* @return void
	*/
	public function listAction() {
		$o_ejc = new EJCModel();
		$o_ejc->getList();

		$o_view = new View('views/ejcList.phtml');
		$o_view->setParams(array('o_ejc' => $o_ejc));
		$o_view->showContents();
	}
}
?>