<?php
global $body_class;
global $head_title;
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $head_title; ?></title>

		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="template/bootstrap/css/cover.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

		<!-- lib -->
		<link rel="stylesheet" type="text/css" href="template/css/lib/normalize.css" />
		<link rel="stylesheet" type="text/css" href="template/css/lib/cbpAnimatedHeader.css" />
		<link rel="stylesheet" type="text/css" href="template/css/lib/gnmenu.css" />
		<link rel="stylesheet" type="text/css" href="template/css/menu.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="template/css/main.css" />
		<link rel="stylesheet" type="text/css" href="template/css/home.css" />


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="<?php echo $body_class; ?> out">
		<section id="container">
			<header id="header">
				<section class="container">
					<ul class="nav">
						<li><a href="?controle=Candidate&acao=requestInfo">Inscrições</a></li>
						<li><a href="javascript://">Sobre nós</a></li>
					</ul>
				</section>
			</header>