<?php
global $body_class;
global $head_title;
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $head_title; ?></title>

		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="template/bootstrap/css/cover.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
		
		<!-- lib -->
		<link rel="stylesheet" type="text/css" href="template/css/lib/normalize.css" />
		<link rel="stylesheet" type="text/css" href="template/css/lib/cbpAnimatedHeader.css" />
		<link rel="stylesheet" type="text/css" href="template/css/lib/gnmenu.css" />
		<link rel="stylesheet" type="text/css" href="template/css/lib/timeline.css" />
		<link rel="stylesheet" type="text/css" href="template/css/lib/font-awesome/font-awesome.css" />
		<link rel="stylesheet" type="text/css" href="template/css/menu.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="template/css/main.css" />


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="<?php echo $body_class; ?>">
		<section id="container">
			<nav>
				<ul id="gn-menu" class="gn-menu-main">
					<li class="gn-trigger">
						<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
						<nav class="gn-menu-wrapper">
							<div class="gn-scroller">
								<ul class="gn-menu">
									<li class="gn-search-item" style="display: none;">
										<input placeholder="Busca" type="search" class="gn-search">
										<a class="gn-icon gn-icon-search"><span>Busca</span></a>
									</li>
									<li>
										<a href="?controle=Member&acao=member" class="icon-user fa-square-o"><?php echo $_SESSION['memberName']; ?></a>
										<ul class="gn-submenu">
											<li><a href="?controle=Config&acao=showConfig" class="gn-icon gn-icon-cog">Painel de controle</a></li>
											<li><a href="?controle=Config&acao=help" class="gn-icon gn-icon-help">Ajuda</a></li>
										</ul>
									</li>
									<li><a href="?controle=Member&acao=list" class="icon-user fa-user">Membros</a></li>
									<li><a href="?controle=EJC&acao=list" class="icon-users fa-users">EJCs</a></li>
									<li><a href="?controle=Menssage&acao=menssage" class="icon-msg fa-comment">Mensagem</a></li>
								</ul>
							</div><!-- /gn-scroller -->
						</nav>
					</li>
					<li></li>
				</ul>
			</nav>
			<header id="header" class="navbar navbar-default navbar-static-top cbp-af-header clear" role="navigation">
				<section class="container cbp-af-inner">
					<h1>
						<a href="?controle=Home&acao=showAdministration">
							<img src="template/images/logo-dove.png" alt="EJC" class="logo-dove">
						</a>
					</h1>
					<div id="logout-mobile">
						<a href="?controle=Login&amp;acao=logout" class="logout"><i class="icon-logout fa-sign-out"></i></a>
					</div>
					<div id="shortcuts">
						<ul>
							<li>
								<a href="?controle=Member&acao=list"><i class="icon-shortcurts fa-user"></i></a>
							</li>
							<li>
								<a href="?controle=Menssage&acao=menssage"><i class="icon-shortcurts fa-comment"></i></a>
							</li>
							<li>
								<a href="?controle=EJC&acao=list"><i class="icon-shortcurts fa-users"></i></a>
							</li>
						</ul>
					</div>
					<div id="user">
						<div class="inner-user">
							<a href="?controle=Member&acao=member">
								<img src="template/images/upload/2655.jpg" class="user-photo">
								<span><?php echo $_SESSION['memberName']; ?></span>
							</a>
							<a href="?controle=Login&acao=logout" class="logout"><i class="icon-logout fa-sign-out"></i></a>
						</div>
					</div>
				</section>
			</header>
