<?php
/**
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/

// Configurando o PHP para mostrar os erros na tela
ini_set('display_errors', 1);
error_reporting(E_ALL);
 
require_once 'lib/Application.php';
$o_Application = new Application();
$o_Application->dispatch();

?>