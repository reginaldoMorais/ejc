<?php
/**
* Responsável por gerenciar e persistir os dados de Contato
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/

/**
* Incluindo classes externas requeridas
*/
require_once 'models/StateModel.php';

class ContactModel extends PersistModelAbstract {
	private $in_id;
	private $in_state;
	private $st_phone;
	private $st_email;
	private $st_address;
	private $st_number;
	private $st_complement;
	private $st_city;
	private $in_status;

	private $o_state;
	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setStateId( $in_state ) {
		$this->in_state = $in_state;
		return $this;
	}
	  

	public function getStateId() {
		return $this->in_state;
	}


	public function setPhone( $st_phone ) {
		$this->st_phone = $st_phone;
		return $this;
	}
	  

	public function getPhone() {
		return $this->st_phone;
	}


	public function setEmail( $st_email ) {
		$this->st_email = $st_email;
		return $this;
	}
	  

	public function getEmail() {
		return $this->st_email;
	}
	  
	  
	public function setAddress( $st_address ) {
		$this->st_address = $st_address;
		return $this;
	}
	  

	public function getAddress() {
		return $this->st_address;
	}
	  

	public function setNumber( $st_number ) {
		$this->st_number = $st_number;
		return $this;
	}
	  

	public function getNumber() {
		return $this->st_number;
	}


	public function setComplement( $st_complement ) {
		$this->st_complement = $st_complement;
		return $this;
	}
	  

	public function getComplement() {
		return $this->st_complement;
	}


	public function setCity( $st_city ) {
		$this->st_city = $st_city;
		return $this;
	}
	  

	public function getCity() {
		return $this->st_city;
	}


	public function setState( $o_state ) {
		$this->o_state = $o_state;
		return $this;
	}
	  

	public function getState() {
		return $this->o_state;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setStateId($o_result->state_id);
		$this->setPhone($o_result->phone);
		$this->setEmail($o_result->email);
		$this->setAddress($o_result->address);
		$this->setNumber($o_result->number);
		$this->setComplement($o_result->complement);
		$this->setCity($o_result->city);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna um Contato através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT c.* 
						FROM ".$this->st_banco.".contact c 
						WHERE c.id = ? 
						LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);

						$o_state = new StateModel();
						$o_state->setAcronym($o_result->acronym);
						$o_state->setCountry($o_result->country);
						$this->setState($o_state);

						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava um novo Contato
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	INSERT INTO ".$this->st_banco.".contact c 
						(c.state_id, c.phone, c.email, c.address, c.number, c.complement, c.city, c.status) 
						VALUES(?, ?, ?, ?, ?, ?, ?, 1)";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getStateId());
			$o_stmt->bindValue(2, $this->getPhone());
			$o_stmt->bindValue(3, $this->getEmail());
			$o_stmt->bindValue(4, $this->getAddress());
			$o_stmt->bindValue(5, $this->getNumber());
			$o_stmt->bindValue(6, $this->getComplement());
			$o_stmt->bindValue(7, $this->getCity());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um Contato
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	UPDATE ".$this->st_banco.".contact c 
						SET c.state_id = ?, 
							c.phone = ?, 
							c.email = ?, 
							c.address = ?, 
							c.number = ?, 
							c.complement = ?, 
							c.city = ? 
						WHERE m.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getStateId());
			$o_stmt->bindValue(2, $this->getPhone());
			$o_stmt->bindValue(3, $this->getEmail());
			$o_stmt->bindValue(4, $this->getAddress());
			$o_stmt->bindValue(5, $this->getNumber());
			$o_stmt->bindValue(6, $this->getComplement());
			$o_stmt->bindValue(7, $this->getCity());
			$o_stmt->bindValue(8, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita um Contato
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE ".$this->st_banco.".contact c 
						SET c.status = 1 
						WHERE c.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um Contato
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE ".$this->st_banco.".contact c 
						SET c.status = 0 
						WHERE c.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>