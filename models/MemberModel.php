<?php
/**
* Responsável por gerenciar e persistir os dados dos Membros
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/

/**
* Incluindo classes externas requeridas
*/
require_once 'models/ContactModel.php';
require_once 'models/ImageModel.php';
require_once 'models/EJCModel.php';
require_once 'models/ParishModel.php';

class MemberModel extends PersistModelAbstract {
	private $in_id;
	private $in_contact;
	private $st_name;
	private $st_email;
	private $st_cellphone;
	private $dt_birthdate;
	private $dt_dateIn;
	private $in_status;

	private $o_contact;
	private $o_image;
	private $o_ejc;
	private $o_memberEJC;
	private $bo_CaseSensitive;

	public $ar_all = array();
	public $ar_image = array();
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setContactId( $in_contact ) {
		$this->in_contact = $in_contact;
		return $this;
	}
	  

	public function getContactId() {
		return $this->in_contact;
	}


	public function setName( $st_name ) {
		$this->st_name = $st_name;
		return $this;
	}
	  

	public function getName() {
		return $this->st_name;
	}
	  
	  
	public function setEmail( $st_email ) {
		$this->st_email = $st_email;
		return $this;
	}
	  

	public function getEmail() {
		return $this->st_email;
	}
	  

	public function setCellphone( $st_cellphone ) {
		$this->st_cellphone = $st_cellphone;
		return $this;
	}
	  

	public function getCellphone() {
		return $this->st_cellphone;
	}


	public function setBirthdate( $dt_birthdate ) {
		$this->dt_birthdate = $dt_birthdate;
		return $this;
	}
	  

	public function getBirthdate() {
		return $this->dt_birthdate;
	}


	public function setDateIn( $dt_dateIn ) {
		$this->dt_dateIn = $dt_dateIn;
		return $this;
	}


	public function getDateIn() {
		return $this->dt_dateIn;
	}


	public function setEJC( $o_ejc ) {
		$this->o_ejc = $o_ejc;
		return $this;
	}
	  

	public function getEJC() {
		return $this->o_ejc;
	}


	public function setContact( $o_contact ) {
		$this->o_contact = $o_contact;
		return $this;
	}
	  

	public function getContact() {
		return $this->o_contact;
	}


	public function setImage( $o_image ) {
		$this->o_image = $o_image;
		return $this;
	}
	  

	public function getImage() {
		return $this->o_image;
	}


	public function setMemberEJC( $o_memberEJC ) {
		$this->o_memberEJC = $o_memberEJC;
		return $this;
	}
	  

	public function getMemberEJC() {
		return $this->o_memberEJC;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setContactId($o_result->contact_id);
		$this->setName($o_result->name);
		$this->setEmail($o_result->email);
		$this->setCellphone($o_result->cellphone);
		$this->setBirthdate($o_result->birthdate);
		$this->setDateIn($o_result->date_in);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna todos os Membros
	* 
	* @param void
	* @return boolean
	*/
	public function getAll() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT m.* 
						FROM member m 
						WHERE m.status = 1 
						LIMIT 1 ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);

			if ($o_stmt->execute()) { 
				if ($o_stmt->rowCount() > 0) { 
					while ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$o_member = new MemberModel();

						$o_member->setId($o_result->id);
						$o_member->setContactId($o_result->contact_id);
						$o_member->setName($o_result->name);
						$o_member->setEmail($o_result->email);
						$o_member->setCellphone($o_result->cellphone);
						$o_member->setBirthdate($o_result->birthdate);
						$o_member->setStatus($o_result->status);

						$this->ar_all[] = $o_member;
					}
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Retorna todos os Membros
	* 
	* @param void
	* @return boolean
	*/
	public function getList() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT m.id, m.name, me.ejc_id, i.url as photo, e.edition, p.nickname 
						FROM ".$this->st_banco.".member m 
							LEFT JOIN ".$this->st_banco.".image i on i.generic_fk = m.id 
								AND i.image_type_id = 1
							JOIN ".$this->st_banco.".member_ejc me on me.member_id = m.id 
							JOIN ".$this->st_banco.".ejc e on e.id = me.ejc_id 
							JOIN ".$this->st_banco.".parish p on p.id = e.parish_id 
						WHERE m.status = 1 
							AND me.action_id = 1 ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);

			if ($o_stmt->execute()) { 
				if ($o_stmt->rowCount() > 0) { 
					while ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 

						$this->o_db = null;
						
						$o_member = new MemberModel();
						$o_ejc = new EJCModel();
						$o_image = new ImageModel();
						$o_parish = new ParishModel();

						$o_member->setId($o_result->id);
						$o_member->setName($o_result->name);

						$o_parish->setNickname($o_result->nickname);

						$o_ejc->setId($o_result->ejc_id);
						$o_ejc->setEdition($o_result->edition);
						$o_ejc->setParish($o_parish);

						if ( $o_result->photo == null)
							$o_image->setUrl("template/images/upload/user-default.jpg");
						else
							$o_image->setUrl($o_result->photo);

						$o_member->setEJC($o_ejc);
						$o_member->setImage($o_image);

						$this->ar_all[] = $o_member;
					}
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Retorna um Membro através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT m.* 
						FROM member m 
						WHERE m.id = ? 
						LIMIT 1 ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);

						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava um novo Membro
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	INSERT INTO member m 
						(m.contact_id, m.name, m.email, m.cellphone, m.birthdate, m.date_in, m.status) 
						VALUES(?, ?, ?, ?, ?, now(), 1) ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getContactId());
			$o_stmt->bindValue(2, $this->getName());
			$o_stmt->bindValue(3, $this->getEmail());
			$o_stmt->bindValue(4, $this->getCellphone());
			$o_stmt->bindValue(5, $this->getBirthdate());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um Membro
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	UPDATE member m 
						SET m.contact_id = ?, 
							m.name = ?, 
							m.email = ?, 
							m.cellphone = ?, 
							m.birthdate = ? 
						WHERE m.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getContactId());
			$o_stmt->bindValue(2, $this->getName());
			$o_stmt->bindValue(3, $this->getEmail());
			$o_stmt->bindValue(4, $this->getCellphone());
			$o_stmt->bindValue(5, $this->getBirthdate());
			$o_stmt->bindValue(6, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita um Membro
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE member m 
						SET m.status = 1 
						WHERE m.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um Membro
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE member m 
						SET m.status = 0 
						WHERE m.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>