<?php
/**
* Responsável por gerenciar e persistir os dados do EJC
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/

/**
* Incluindo classes externas requeridas
*/
require_once 'models/ParishModel.php';

class EJCModel extends PersistModelAbstract {
	private $in_id;
	private $in_parish;
	private $in_local;
	private $in_edition;
	private $st_theme;
	private $st_music;
	private $st_tshirt;
	private $st_patronSaint;
	private $dt_date;
	private $in_year;
	private $in_status;

	private $o_parish;
	private $bo_CaseSensitive;

	public $ar_all = array();
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setParishId( $in_parish ) {
		$this->in_parish = $in_parish;
		return $this;
	}
	  

	public function getParishId() {
		return $this->in_parish;
	}


	public function setLocalId( $in_local ) {
		$this->in_local = $in_local;
		return $this;
	}
	  

	public function getLocalId() {
		return $this->in_local;
	}


	public function setEdition( $in_edition ) {
		$this->in_edition = $in_edition;
		return $this;
	}
	  

	public function getEdition() {
		return $this->in_edition;
	}


	public function setTheme( $st_theme ) {
		$this->st_theme = $st_theme;
		return $this;
	}
	  

	public function getTheme() {
		return $this->st_theme;
	}


	public function setMusic( $st_music ) {
		$this->st_music = $st_music;
		return $this;
	}
	  

	public function getMusic() {
		return $this->st_music;
	}


	public function setTShirt( $st_tshirt ) {
		$this->st_tshirt = $st_tshirt;
		return $this;
	}
	  

	public function getTShirt() {
		return $this->st_tshirt;
	}


	public function setPatronSaint( $st_patronSaint ) {
		$this->st_patronSaint = $st_patronSaint;
		return $this;
	}
	  

	public function getPatronSaint() {
		return $this->st_patronSaint;
	}


	public function setDate( $dt_date ) {
		$this->dt_date = $dt_date;
		return $this;
	}
	  

	public function getDate() {
		return $this->dt_date;
	}


	public function setYear( $in_year ) {
		$this->in_year = $in_year;
		return $this;
	}
	  

	public function getYear() {
		return $this->in_year;
	}


	public function setParish( $o_parish ) {
		$this->o_parish = $o_parish;
		return $this;
	}
	  

	public function getParish() {
		return $this->o_parish;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setParishId($o_result->parish_id);
		$this->setLocalId($o_result->local_id);
		$this->setEdition($o_result->edition);
		$this->setTheme($o_result->theme);
		$this->setMusic($o_result->music);
		$this->setTShirt($o_result->tshirt);
		$this->setPatronSaint($o_result->patron_saint);
		$this->setDate($o_result->date);
		$this->setYear($o_result->year);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna um EJC através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "SELECT e.* FROM ejc e WHERE e.id = ? LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Retorna todos os EJCs
	* 
	* @param void
	* @return boolean
	*/
	public function getList() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT 
						e.id, 
						e.edition, 
						e.tshirt, 
						p.id as 'parishId', 
						p.name as 'parishName', 
						p.nickname as 'parishNickname'
						FROM ".$this->st_banco.".ejc e
							JOIN ".$this->st_banco.".parish p on p.id = e.parish_id 
						WHERE e.status = 1 
						ORDER BY e.id DESC ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);

			if ($o_stmt->execute()) { 
				if ($o_stmt->rowCount() > 0) { 
					while ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$o_ejc = new EJCModel();
						$o_parish = new ParishModel();

						$o_ejc->setId($o_result->id);
						$o_ejc->setEdition($o_result->edition);
						$o_ejc->setTShirt($o_result->tshirt);

						$o_parish->setId($o_result->parishId);
						$o_parish->setName($o_result->parishName);
						$o_parish->setNickname($o_result->parishNickname);
						$o_ejc->setParish($o_parish);

						if ( $o_result->tshirt == null)
							$o_ejc->setTShirt("#5B5B5B");
						else
							$o_ejc->setTShirt($o_result->tshirt);

						$this->ar_all[] = $o_ejc;
					}
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava um novo EJC
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "INSERT INTO ejc e (e.parish_id, e.local_id, e.edition, e.theme, e.music, e.tshirt, e.patron_saint, e.date, e.year) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, 1)";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getParishId());
			$o_stmt->bindValue(2, $this->getLocalId());
			$o_stmt->bindValue(3, $this->getEdition());
			$o_stmt->bindValue(4, $this->getTheme());
			$o_stmt->bindValue(5, $this->getMusic());
			$o_stmt->bindValue(6, $this->getTShirt());
			$o_stmt->bindValue(7, $this->getPatronSaint());
			$o_stmt->bindValue(8, $this->getDate());
			$o_stmt->bindValue(9, $this->getYear());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um EJC
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "UPDATE ejc e SET e.parish_id = ?, e.local_id = ?, e.edition = ?, e.theme = ?, e.music = ?, e.tshirt = ?, e.patron_saint = ?, e.date = ?, e.year = ? WHERE e.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getParishId());
			$o_stmt->bindValue(2, $this->getLocalId());
			$o_stmt->bindValue(3, $this->getEdition());
			$o_stmt->bindValue(4, $this->getTheme());
			$o_stmt->bindValue(5, $this->getMusic());
			$o_stmt->bindValue(6, $this->getTShirt());
			$o_stmt->bindValue(7, $this->getPatronSaint());
			$o_stmt->bindValue(8, $this->getDate());
			$o_stmt->bindValue(9, $this->getYear());
			$o_stmt->bindValue(10, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita um EJC
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE ejc e SET e.status = 1 WHERE e.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um EJC
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE ejc e SET e.status = 0 WHERE e.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>