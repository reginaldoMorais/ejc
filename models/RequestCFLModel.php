<?php
/**
* Responsável por gerenciar e persistir os dados de Ficha de Inscrição
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class RequestCFLModel extends PersistModelAbstract {
	private $in_id;
	private $in_member;
	private $in_inscription_cfl;
	private $in_status;

	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setMember( $in_member ) {
		$this->in_member = $in_member;
		return $this;
	}
	  

	public function getMember() {
		return $this->in_member;
	}


	public function setInscription( $in_inscription_cfl ) {
		$this->in_inscription_cfl = $in_inscription_cfl;
		return $this;
	}
	  

	public function getInscription() {
		return $this->in_inscription_cfl;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setCandidate($o_result->member_id);
		$this->getInscription($o_result->inscription_cfl_id);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna uma Ficha de Inscrição através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "SELECT f.* FROM form f WHERE f.id = ? LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava uma nova Ficha de Inscrição
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "INSERT INTO form f (i.member_id, f.inscription_cfl_id) VALUES(?, ?, 1)";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMember());
			$o_stmt->bindValue(2, $this->getInscription());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de uma Ficha de Inscrição
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "UPDATE form f SET f.member_id = ?, f.inscription_cfl_id = ? WHERE f.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMember());
			$o_stmt->bindValue(2, $this->getInscription());
			$o_stmt->bindValue(3, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita uma Ficha de Inscrição
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE form f SET f.status = 1 WHERE f.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita uma Ficha de Inscrição
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE form f SET f.status = 0 WHERE f.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>