<?php
/**
* Responsável por gerenciar e persistir os dados do Local
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class LocalModel extends PersistModelAbstract {
	private $in_id;
	private $in_contact;
	private $st_name;
	private $st_description;
	private $in_status;

	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setContact( $in_contact ) {
		$this->in_contact = $in_contact;
		return $this;
	}
	  

	public function getContact() {
		return $this->in_contact;
	}


	public function setName( $st_name ) {
		$this->st_name = $st_name;
		return $this;
	}
	  

	public function getName() {
		return $this->st_name;
	}
	  
	  
	public function setDescription( $st_description ) {
		$this->st_description = $st_description;
		return $this;
	}
	  

	public function getDescription() {
		return $this->st_description;
	}
	  

	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setContact($o_result->contact_id);
		$this->setName($o_result->name);
		$this->setDescription($o_result->description);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna um Local através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "SELECT l.* FROM local l WHERE l.id = ? LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava um novo Local
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "INSERT INTO local l (l.contact_id, l.name, l.description) VALUES(?, ?, 1)";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getContact());
			$o_stmt->bindValue(2, $this->getName());
			$o_stmt->bindValue(3, $this->getDescription());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um Local
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "UPDATE local l SET l.contact_id, l.name = ?, l.description = ? WHERE l.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getContact());
			$o_stmt->bindValue(2, $this->getName());
			$o_stmt->bindValue(3, $this->getDescription());
			$o_stmt->bindValue(4, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita uma Local
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE local l SET l.status = 1 WHERE l.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um Local
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE local l SET l.status = 0 WHERE l.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>