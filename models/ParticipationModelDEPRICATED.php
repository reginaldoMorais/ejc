<?php
/**
* Responsável por gerenciar e persistir os dados da Participação
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/

/**
* Incluindo classes externas requeridas
*/
require_once 'models/TeamModel.php';
require_once 'models/AreaModel.php';
require_once 'models/ActionModel.php';

class ParticipationModel extends PersistModelAbstract {
	private $in_id;
	private $in_team;
	private $in_area;
	private $in_action;
	private $dt_date;
	private $in_status;

	private $o_team;
	private $o_area;
	private $o_action;
	private $bo_CaseSensitive;


	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setTeamId($o_result->team_id);
		$this->setAreaId($o_result->area_id);
		$this->setActionId($o_result->action_id);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna uma Participação através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT p.* 
						FROM participation p 
						WHERE p.id = ? 
						LIMIT 1 ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Retorna todas as Participações através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getAllById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT 
						p.*, 
						t.name as 'nameTeam', 
						ar.name as 'nameArea', 
						ac.name as 'nameAction' 
						FROM ".$this->st_banco.".participation p 
							JOIN ".$this->st_banco.".team t on t.id = p.team_id 
							JOIN ".$this->st_banco.".area ar on a.id = p.area_id 
							JOIN ".$this->st_banco.".action ac on ac.id = p.action_id 
						WHERE p.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);

						$o_team = new TeamModel();
						$o_area = new AreaModel();
						$o_action = new ActionModel();

						$o_team->setName($o_result->nameTeam);
						$o_area->setName($o_result->nameArea);
						$o_action->setName($o_result->nameAction);

						$this->setTeam($o_team);
						$this->setArea($o_area);
						$this->setAction($o_action);

						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava uma nova Participação
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	INSERT INTO participation p 
						(p.team_id, p.area_id, p.action_id = ?, p.date) 
						VALUES(?, ?, ?, 1) ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getTeamId());
			$o_stmt->bindValue(2, $this->getAreaId());
			$o_stmt->bindValue(3, $this->getActionId());
			$o_stmt->bindValue(4, $this->getDateId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de uma Participação
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	UPDATE participation p 
						SET p.team_id = ?, p.area_id = ?, p.action_id = ?, p.date = ? 
						WHERE p.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getTeamId());
			$o_stmt->bindValue(2, $this->getAreaId());
			$o_stmt->bindValue(3, $this->getActionId());
			$o_stmt->bindValue(4, $this->getDate());
			$o_stmt->bindValue(5, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) {
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita uma Participação
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE participation p 
						SET p.status = 1 
						WHERE p.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita uma Participação
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE participation p 
						SET p.status = 0 
						WHERE p.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>