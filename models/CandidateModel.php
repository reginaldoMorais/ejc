<?php
/**
* Responsável por gerenciar e persistir os dados dos Candidatos
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class CandidateModel extends PersistModelAbstract {
	private $in_id;
	private $in_contact;
	private $st_name;
	private $st_email;
	private $st_cellphone;
	private $dt_birthdate;
	private $dt_dateIn;
	private $in_status;

	private $ar_all;
	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setContact( $in_contact ) {
		$this->in_contact = $in_contact;
		return $this;
	}
	  

	public function getContact() {
		return $this->in_contact;
	}


	public function setName( $st_name ) {
		$this->st_name = $st_name;
		return $this;
	}
	  

	public function getName() {
		return $this->st_name;
	}
	  
	  
	public function setEmail( $st_email ) {
		$this->st_email = $st_email;
		return $this;
	}
	  

	public function getEmail() {
		return $this->st_email;
	}
	  

	public function setCellphone( $st_cellphone ) {
		$this->st_cellphone = $st_cellphone;
		return $this;
	}
	  

	public function getCellphone() {
		return $this->st_cellphone;
	}


	public function setBirthdate( $dt_birthdate ) {
		$this->dt_birthdate = $dt_birthdate;
		return $this;
	}
	  

	public function getBirthdate() {
		return $this->dt_birthdate;
	}


	public function setDateIn( $dt_dateIn ) {
		$this->dt_dateIn = $dt_dateIn;
		return $this;
	}

	public function getDateIn() {
		return $this->dt_dateIn;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setContact($o_result->contact_id);
		$this->setName($o_result->name);
		$this->setEmail($o_result->email);
		$this->setCellphone($o_result->cellphone);
		$this->setBirthdate($o_result->birthdate);
		$this->setDateIn($o_result->date_in);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna todas os Candidatos
	* 
	* @param void
	* @return boolean
	*/
	public function getAll() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT p.* 
						FROM ".$this->st_banco.".permission p 
						WHERE p.status = 1 
						LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);

			if ($o_stmt->execute()) { 
				if ($o_stmt->rowCount() > 0) { 
					while ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$o_member = new Member();
						
						$o_member->setId($o_result->id);
						$o_member->setContact($o_result->contact_id);
						$o_member->setName($o_result->name);
						$o_member->setEmail($o_result->email);
						$o_member->setCellphone($o_result->cellphone);
						$o_member->setBirthdate($o_result->birthdate);
						$o_member->setStatus($o_result->status);

						$this->ar_all->put($o_member);
					}
				}
			}

			return true;
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Retorna um Candidatos através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT c.* 
						FROM ".$this->st_banco.".candidate c 
						WHERE c.id = ? 
						LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava um novo Candidato
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	INSERT INTO ".$this->st_banco.".candidate c 
						(c.contact_id, c.name, c.email, c.cellphone, c.birthdate, c.date_in, c.status) 
						VALUES(?, ?, ?, ?, ?, now(), 1)";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getContact());
			$o_stmt->bindValue(2, $this->getName());
			$o_stmt->bindValue(3, $this->getEmail());
			$o_stmt->bindValue(4, $this->getCellphone());
			$o_stmt->bindValue(5, $this->getBirthdate());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um Candidato
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	UPDATE ".$this->st_banco.".candidate c 
						SET c.contact_id = ?, 
							c.name = ?, 
							c.email = ?,
							c.cellphone = ?, 
							c.birthdate = ? 
						WHERE c.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getContact());
			$o_stmt->bindValue(2, $this->getName());
			$o_stmt->bindValue(3, $this->getEmail());
			$o_stmt->bindValue(4, $this->getCellphone());
			$o_stmt->bindValue(5, $this->getBirthdate());
			$o_stmt->bindValue(6, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita um Candidato
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE ".$this->st_banco.".candidate c 
						SET c.status = 1 
						WHERE c.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um Candidato
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE ".$this->st_banco.".candidate c 
						SET c.status = 0 
						WHERE c.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>