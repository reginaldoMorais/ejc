<?php
/**
* Responsável por gerenciar e persistir os dados do CFL
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class CFLModel extends PersistModelAbstract {
	private $in_id;
	private $in_ejc;
	private $in_local;
	private $st_theme;
	private $dt_date;
	private $in_status;

	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setEJCId( $in_ejc ) {
		$this->in_ejc = $in_ejc;
		return $this;
	}
	  

	public function getEJCId() {
		return $this->in_ejc;
	}


	public function setLocalId( $in_local ) {
		$this->in_local = $in_local;
		return $this;
	}
	  

	public function getLocalId() {
		return $this->in_local;
	}


	public function setTheme( $st_theme ) {
		$this->st_theme = $st_theme;
		return $this;
	}
	  

	public function getTheme() {
		return $this->st_theme;
	}


	public function setDate( $dt_date ) {
		$this->dt_date = $dt_date;
		return $this;
	}


	public function getDate() {
		return $this->dt_date;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	} 
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setEJCId($o_result->ejc_id);
		$this->setLocalId($o_result->local_id);
		$this->setTheme($o_result->theme);
		$this->setDate($o_result->date);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna um CFL através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "SELECT c.* FROM cfl c WHERE c.id = ? LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava um novo CFL
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "INSERT INTO cfl c (c.ejc_id, c.local_id, c.theme, c.date, c.status) VALUES(?, ?, ?, ?, 1)";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getEJCId());
			$o_stmt->bindValue(2, $this->getLocalId());
			$o_stmt->bindValue(2, $this->getTheme());
			$o_stmt->bindValue(2, $this->getDate());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um CFL
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "UPDATE cfl c SET c.ejc_id = ?, c.local_id = ?, c.theme = ?, c.date = ? WHERE a.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getEJCId());
			$o_stmt->bindValue(2, $this->getLocalId());
			$o_stmt->bindValue(3, $this->getTheme());
			$o_stmt->bindValue(4, $this->getDate());
			$o_stmt->bindValue(5, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita um CFL
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE cfl c SET c.status = 1 WHERE c.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um CFL
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE cfl c SET c.status = 0 WHERE c.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>