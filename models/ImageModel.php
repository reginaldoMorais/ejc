<?php
/**
* Responsável por gerenciar e persistir os dados da Imagem
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/

/**
* Incluindo classes externas requeridas
*/
require_once 'models/ImageTypeModel.php';
require_once 'models/EntityImageModel.php';

class ImageModel extends PersistModelAbstract {
	private $in_id;
	private $in_type;
	private $in_entity;
	private $in_generic;
	private $st_url;
	private $st_description;
	private $in_status;

	private $o_type;
	private $o_entity;
	private $bo_CaseSensitive;

	public $ar_all = array();
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setTypeId( $in_type ) {
		$this->in_type = $in_type;
		return $this;
	}
	  

	public function getTypeId() {
		return $this->in_type;
	}


	public function setEntityId( $in_entity ) {
		$this->in_entity = $in_entity;
		return $this;
	}
	  

	public function getEntityId() {
		return $this->in_entity;
	}


	public function setGeneric( $in_generic ) {
		$this->in_generic = $in_generic;
		return $this;
	}
	  

	public function getGeneric() {
		return $this->in_generic;
	}


	public function setURL( $st_url ) {
		$this->st_url = $st_url;
		return $this;
	}
	  

	public function getURL() {
		return $this->st_url;
	}


	public function setDescription( $st_description ) {
		$this->st_description = $st_description;
		return $this;
	}
	  

	public function getDescription() {
		return $this->st_description;
	}


	public function setType( $o_type ) {
		$this->o_type = $o_type;
		return $this;
	}
	  

	public function getType() {
		return $this->o_type;
	}


	public function setEntity( $o_entity ) {
		$this->o_entity = $o_entity;
		return $this;
	}
	  

	public function getEntity() {
		return $this->o_entity;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setTypeId($o_result->image_type_id);
		$this->setEntityId($o_result->entity_image_id);
		$this->setGeneric($o_result->generic_fk);
		$this->setURL($o_result->url);
		$this->setDescription($o_result->description);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna uma Imagem através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "SELECT i.* 
						FROM ".$this->st_banco.".image i 
						WHERE i.id = ? 
						LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}

	/**
	* Retorna uma lista de Imagens através de um ID de membro
	* 
	* @param void
	* @return boolean
	*/
	public function getAllByMemberId($in_member_id) {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_entity = 1;

		$st_sql = "	SELECT i.*
						FROM ".$this->st_banco.".image i
						WHERE i.generic_fk = ? 
							AND i.entity_image_id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $in_member_id);
			$o_stmt->bindValue(2, $in_entity);

			if ($o_stmt->execute()) { 
				if ($o_stmt->rowCount() > 0) { 
					while ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) {
						$o_image = new ImageModel();

						$o_image->setId($o_result->id);
						$o_image->setTypeId($o_result->image_type_id);
						$o_image->setEntityId($o_result->entity_image_id);
						$o_image->setGeneric($o_result->generic_fk);
						$o_image->setURL($o_result->url);
						$o_image->setDescription($o_result->description);
						$o_image->setStatus($o_result->status);
									

						$this->ar_all[] = $o_image;
					}
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>