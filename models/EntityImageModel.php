<?php
/**
* Responsável por gerenciar e persistir os dados de Entidade de Imagem
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class EntityImageModel extends PersistModelAbstract {
	private $in_id;
	private $st_name;
	private $st_description;
	private $in_status;

	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setName( $st_name ) {
		$this->st_name = $st_name;
		return $this;
	}
	  

	public function getName() {
		return $this->st_name;
	}
	  
	  
	public function setDescription( $st_description ) {
		$this->st_description = $st_description;
		return $this;
	}
	  

	public function getDescription() {
		return $this->st_description;
	}
	  

	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setName($o_result->name);
		$this->setDescription($o_result->description);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna uma Entidade de Imagem através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "SELECT ei.* FROM entity_image ei WHERE ei.id = ? LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>