<?php
/**
* Responsável por gerenciar e persistir os dados do Membro no EJC
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/

/**
* Incluindo classes externas requeridas
*/
require_once 'models/TeamModel.php';
require_once 'models/AreaModel.php';
require_once 'models/ActionModel.php';

class MemberEJCModel extends PersistModelAbstract {
	private $in_id;
	private $in_member;
	private $in_ejc;
	private $in_team;
	private $in_area;
	private $in_action;
	private $dt_date;
	private $in_status;

	private $o_team;
	private $o_area;
	private $o_action;
	private $bo_CaseSensitive;

	private $st_date;
	private $st_ejc;
	private $st_parish;
	private $st_parish_nickname;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setMemberId( $in_member ) {
		$this->in_member = $in_member;
		return $this;
	}
	  

	public function getMemberId() {
		return $this->in_member;
	}


	public function setEJCId( $in_ejc ) {
		$this->in_ejc = $in_ejc;
		return $this;
	}
	  

	public function getEJCId() {
		return $this->in_ejc;
	}


	public function setTeamId( $in_team ) {
		$this->in_team = $in_team;
		return $this;
	}
	  

	public function getTeamId() {
		return $this->in_team;
	}


	public function setAreaId( $in_area ) {
		$this->in_area = $in_area;
		return $this;
	}
	  

	public function getAreaId() {
		return $this->in_area;
	}


	public function setActionId( $in_action ) {
		$this->in_action = $in_action;
		return $this;
	}
	  

	public function getActionId() {
		return $this->in_action;
	}


	public function setTeam( $o_team ) {
		$this->o_team = $o_team;
		return $this;
	}
	  

	public function getTeam() {
		return $this->o_team;
	}


	public function setArea( $o_area ) {
		$this->o_area = $o_area;
		return $this;
	}
	  

	public function getArea() {
		return $this->o_area;
	}


	public function setAction( $o_action ) {
		$this->o_action = $o_action;
		return $this;
	}
	  

	public function getAction() {
		return $this->o_action;
	}


	public function setDate( $dt_date ) {
		$this->dt_date = $dt_date;
		return $this;
	}
	  

	public function getDate() {
		return $this->dt_date;
	}


	public function setDateSt( $st_date ) {
		$timestamp = strtotime($st_date);
		$this->st_date = date('d/m/Y', $timestamp);
		return $this;
	}
	  

	public function getDateSt() {
		return $this->st_date;
	}


	public function setEJC( $st_ejc ) {
		$this->st_ejc = $st_ejc;
		return $this;
	}
	  

	public function getEJC() {
		return $this->st_ejc;
	}


	public function setParish( $st_parish ) {
		$this->st_parish = $st_parish;
		return $this;
	}
	  

	public function getParish() {
		return $this->st_parish;
	}


	public function setParishNickname( $st_parish_nickname ) {
		$this->st_parish_nickname = $st_parish_nickname;
		return $this;
	}
	  

	public function getParishNickname() {
		return $this->st_parish_nickname;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setMemberId($o_result->member_id);
		$this->setEJCId($o_result->ejc_id);
		$this->setTeamId($o_result->team_id);
		$this->setAreaId($o_result->area_id);
		$this->setActionId($o_result->action_id);
		$this->setDate($o_result->date);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna os dados de um Membro no EJC através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT me.* 
						FROM ".$this->st_banco.".member_ejc me 
						WHERE me.id = ? 
						LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Retorna os dados de um Membro no EJC através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getAllByMemberId($in_member_id) {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT 
						me.*,
						t.id as 'teamId', 
						t.name as 'teamName', 
						ar.id as 'areaId', 
						ar.name as 'areaName',  
						ac.id as 'actionId', 
						ac.name as 'actionName', 
						e.edition, 
						pr.name as 'parishName',
						pr.nickname as 'parishNickname'
						FROM ".$this->st_banco.".member_ejc me 
							JOIN ".$this->st_banco.".member m on m.id = me.member_id
							JOIN ".$this->st_banco.".ejc e on e.id = me.ejc_id
							JOIN ".$this->st_banco.".parish pr on pr.id = e.parish_id
							JOIN ".$this->st_banco.".team t on t.id = me.team_id
							JOIN ".$this->st_banco.".area ar on ar.id = me.area_id
							JOIN ".$this->st_banco.".action ac on ac.id = me.action_id
						WHERE m.id = ?
						ORDER BY me.date DESC ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $in_member_id);

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					while ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$o_memberEJC = new MemberEJCModel();
						$o_team = new TeamModel();
						$o_area = new AreaModel();
						$o_action = new ActionModel();

						$o_memberEJC->setId($o_result->id);
						$o_memberEJC->setMemberId($o_result->member_id);
						$o_memberEJC->setEJCId($o_result->ejc_id);
						$o_memberEJC->setTeamId($o_result->team_id);
						$o_memberEJC->setAreaId($o_result->area_id);
						$o_memberEJC->setActionId($o_result->action_id);
						$o_memberEJC->setDate($o_result->date);
						$o_memberEJC->setDateSt($o_result->date);
						$o_memberEJC->setStatus($o_result->status);

						$o_team->setId($o_result->teamId);
						$o_team->setName($o_result->teamName);
						$o_memberEJC->setTeam($o_team);

						$o_area->setId($o_result->areaId);
						$o_area->setName($o_result->areaName);
						$o_memberEJC->setArea($o_area);

						$o_action->setId($o_result->actionId);
						$o_action->setName($o_result->actionName);
						$o_memberEJC->setAction($o_action);

						$o_memberEJC->setEJC($o_result->edition);
						$o_memberEJC->setParish($o_result->parishName);
						$o_memberEJC->setParishNickname($o_result->parishNickname);

						$this->ar_all[] = $o_memberEJC;
					}
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava um novo Membro no EJC
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	INSERT INTO member_ejc me 
						(me.member_id, me.ejc_id, me.team_id, me.area_id, me.action_id, me.date)  
						VALUES(?, ?, ?, ?, ?, ?, 1)";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMemberId());
			$o_stmt->bindValue(2, $this->getEJCId());
			$o_stmt->bindValue(3, $this->getTeamId());
			$o_stmt->bindValue(4, $this->getAreaId());
			$o_stmt->bindValue(5, $this->getActionId());
			$o_stmt->bindValue(6, $this->getDateId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um Membro no EJC
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	UPDATE member_ejc me 
						SET p.team_id = ?, 
							p.area_id = ?, 
							p.action_id = ?, 
							p.date = ?  
						WHERE me.member_id = ? 
							AND me.ejc_id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getTeamId());
			$o_stmt->bindValue(2, $this->getAreaId());
			$o_stmt->bindValue(3, $this->getActionId());
			$o_stmt->bindValue(4, $this->getDate());
			$o_stmt->bindValue(5, $this->getMemberId());
			$o_stmt->bindValue(6, $this->getEJCId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) {
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita um Membro no EJC
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE member_ejc me 
						SET p.status = 1 
						WHERE me.member_id = ? 
							AND me.ejc_id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMemberId());
			$o_stmt->bindValue(1, $this->getEJCId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um Membro no EJC
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE member_ejc me 
						SET p.status = 0 
						WHERE me.member_id = ? 
							AND me.ejc_id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMemberId());
			$o_stmt->bindValue(1, $this->getEJCId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>