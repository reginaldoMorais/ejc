<?php
/**
* Responsável por gerenciar e persistir os dados de Login
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class LoginModel extends PersistModelAbstract {
	private $in_id;
	private $in_member;
	private $in_permission;
	private $st_login;
	private $st_password;
	private $st_name;
	private $dt_dateIn;
	private $in_status;
	
	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setMemberId( $in_member ) {
		$this->in_member = $in_member;
		return $this;
	}
	  

	public function getMemberId() {
		return $this->in_member;
	}


	public function setPermissionId( $in_permission ) {
		$this->in_permission = $in_permission;
		return $this;
	}
	  

	public function getPermissionId() {
		return $this->in_permission;
	}
	  

	public function setLogin( $st_login ) {
		$this->st_login = $st_login;
		return $this;
	}
	  

	public function getLogin() {
		return $this->st_login;
	}
	  

	public function setPassword( $st_password ) {
		$this->st_password = $st_password;
		return $this;
	}
	  

	public function getPassword() {
		return $this->st_password;
	}


	public function setName( $st_name ) {
		$this->st_name = $st_name;
		return $this;
	}
	  

	public function getName() {
		return $this->st_name;
	}

	public function setDateIn( $dt_dateIn ) {
		$this->dt_dateIn = $dt_dateIn;
		return $this;
	}


	public function getDateIn() {
		return $this->dt_dateIn;
	}
	  

	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setPermissionId($o_result->nivel);
		$this->setName($o_result->nome);
		$this->setLogin($o_result->login);
		$this->setDateIn($o_result->date_in);
		$this->setStatus($o_result->status);
	}


	/**
	* Verifica o login do membro
	* 
	* @param void
	* @return boolean
	*/
	public function validateUser() {		
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT 
						l.id, 
						l.name, 
						l.permission_id 
						FROM ".$this->st_banco.".login l 
						WHERE l.login = ? 
							AND l.password = ? 
							AND l.status = 1 
						LIMIT 1 ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getLogin());
			$o_stmt->bindValue(2, sha1($this->getPassword()));

			if ($o_stmt->execute()) { 
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						
						$this->setId($o_result->id);
						$this->setPermissionId($o_result->permission_id);
						$this->setName($o_result->name);

						$o_security = new Security();
						$o_security->setSessionParameters( $this );

						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Efetua o logout do sistema
	* 
	* @param void
	* @return void
	*/
	public function logout() {
		$o_security = new Security();
		$o_security->logout();
	}


	/**
	* Grava um novo Login
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	INSERT INTO login l 
						(l.member_id, l.permission_id, l.login, l.password, l.name, l.date_in, l.status) 
						VALUES(?, ?, ?, ?, ?, now(), 1) ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMemberId());
			$o_stmt->bindValue(2, $this->getPermissionId());
			$o_stmt->bindValue(3, $this->getLogin());
			$o_stmt->bindValue(4, $this->getPassword());
			$o_stmt->bindValue(5, $this->getName());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um Login
	* 
	* @param void
	* @return boolean
	*/
	public function changePassword() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	UPDATE login l 
						SET l.password = ? 
						WHERE c.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, sha1($this->getPassword()));
			$o_stmt->bindValue(2, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita um Login
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE login l 
						SET l.status = 1 
						WHERE l.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um Login
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE login l 
						SET l.status = 0 
						WHERE l.id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>