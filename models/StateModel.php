<?php
/**
* Responsável por gerenciar e persistir os dados de Estados
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class StateModel extends PersistModelAbstract {
	private $in_id;
	private $st_name;
	private $st_acronym;
	private $st_country;
	private $in_status;

	private $ar_all;
	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setName( $st_name ) {
		$this->st_name = $st_name;
		return $this;
	}
	  

	public function getName() {
		return $this->st_name;
	}
	  
	  
	public function setAcronym( $st_acronym ) {
		$this->st_acronym = $st_acronym;
		return $this;
	}
	  

	public function getAcronym() {
		return $this->st_acronym;
	}
	  

	public function setCountry( $st_country ) {
		$this->st_country = $st_country;
		return $this;
	}
	  

	public function getCountry() {
		return $this->st_country;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setName($o_result->name);
		$this->setAcronym($o_result->acronym);
		$this->setCountry($o_result->country);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna todas os Estados
	* 
	* @param void
	* @return boolean
	*/
	public function getAll() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT s.* 
						FROM ".$this->st_banco.".state s 
						WHERE s.status = 1 
						LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);

			if ($o_stmt->execute()) { 
				if ($o_stmt->rowCount() > 0) { 
					while ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$o_state = new State();

						$o_state->setId($o_result->id);
						$o_state->setName($o_result->name);
						$o_state->setAcronym($o_result->acronym);
						$o_state->setCountry($o_result->country);
						$o_state->setStatus($o_result->status);

						$this->ar_all->put($o_state);
					}
				}
			}

			return true;
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Retorna um Estados através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT s.* 
						FROM ".$this->st_banco.".state s 
						WHERE s.id = ? 
						LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>