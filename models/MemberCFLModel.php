<?php
/**
* Responsável por gerenciar e persistir os dados do Membro no CFL
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/

/**
* Incluindo classes externas requeridas
*/
require_once 'models/CFLModel.php';
require_once 'models/TaskModel.php';

class MemberCFLModel extends PersistModelAbstract {
	private $in_id;
	private $in_member;
	private $in_cfl;
	private $in_task;
	private $in_status;

	private $o_task;
	private $bo_CaseSensitive;


	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setMember( $in_member ) {
		$this->in_member = $in_member;
		return $this;
	}
	  

	public function getMember() {
		return $this->in_member;
	}


	public function setCFL( $in_cfl ) {
		$this->in_cfl = $in_cfl;
		return $this;
	}
	  

	public function getCFL() {
		return $this->in_cfl;
	}


	public function setTaskId( $in_task ) {
		$this->in_task = $in_task;
		return $this;
	}
	  

	public function getTaskId() {
		return $this->in_task;
	}


	public function setTask( $o_task ) {
		$this->o_task = $o_task;
		return $this;
	}
	  

	public function getTask() {
		return $this->o_task;
	}


	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}
	  

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setMember($o_result->member_id);
		$this->setCFL($o_result->cfl_id);
		$this->setTaskId($o_result->task_id);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna os dados de um Membro no CFL através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	SELECT mc.* 
						FROM ".$this->st_banco.".member_cfl mc 
						WHERE mc.id = ? 
						LIMIT 1 ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava um novo Membro no EJC
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	INSERT INTO member_cfl mc 
						(mc.member_id, mc.cfl_id, mc.task_id) 
						VALUES(?, ?, ?, 1) ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMember());
			$o_stmt->bindValue(2, $this->getCFL());
			$o_stmt->bindValue(3, $this->getTask());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de um Membro no EJC
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "	UPDATE member_cfl mc 
						SET mc.task_id = ? 
						WHERE mc.member_id = ? 
							AND mc.cfl_id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getTask());
			$o_stmt->bindValue(2, $this->getMember());
			$o_stmt->bindValue(3, $this->getCFL());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) {
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Habilita um Membro no EJC
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE member_cfl mc 
						SET mc.status = 1 
						WHERE mc.member_id = ? 
							AND mc.cfl_id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMember());
			$o_stmt->bindValue(1, $this->getCFL());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita um Membro no EJC
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "	UPDATE member_cfl mc 
						SET mc.status = 0 
						WHERE mc.member_id = ? 
							AND mc.ejc_id = ? ";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getMember());
			$o_stmt->bindValue(1, $this->getCFL());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>