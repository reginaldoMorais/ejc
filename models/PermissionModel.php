<?php
/**
* Responsável por gerenciar e persistir os dados de Permissão dos Membros
* 
* @package EJC
* @author Reginaldo Morais
* @author Fabiano Morais
* @version 0.0.1
*/
class PermissionModel extends PersistModelAbstract {
	private $in_id;
	private $st_name;
	private $tx_description;
	private $in_status;

	private $ar_all;
	private $bo_CaseSensitive;
	  

	public function __construct() {
		parent::__construct();
	}


	/**
	* Setters e Getters
	*/
	  
	public function setId( $in_id ) {
		$this->in_id = $in_id;
		return $this;
	}
	  

	public function getId() {
		return $this->in_id;
	}


	public function setName( $st_name ) {
		$this->st_name = $st_name;
		return $this;
	}
	  

	public function getName() {
		return $this->st_name;
	}


	public function setDescription( $tx_description ) {
		$this->tx_description = $tx_description;
		return $this;
	}
	  

	public function getDescription() {
		return $this->tx_description;
	}
	  

	public function setStatus( $in_status ) {
		$this->in_status = $in_status;
		return $this;
	}

	public function getStatus() {
		return $this->in_status;
	}


	public function isCaseSensitive() {
		$this->bo_CaseSensitive = false;
		return $this->bo_CaseSensitive;
	}


	/**
	* Recupera os dados da consulta e inserem no modelo
	* 
	* @param object
	* @return object
	*/
	private function getRow($o_result) {
		$this->setId($o_result->id);
		$this->setName($o_result->name);
		$this->setDescription($o_result->description);
		$this->setStatus($o_result->status);
	}


	/**
	* Retorna todas as permissões
	* 
	* @param void
	* @return boolean
	*/
	public function getAll() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "SELECT p.* FROM permission p WHERE p.status = 1 LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);

			if ($o_stmt->execute()) { 
				if ($o_stmt->rowCount() > 0) { 
					while ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$o_permission = new Permission();

						$o_permission->setId($o_result->id);
						$o_permission->setName($o_result->name);
						$o_permission->setDescription($o_result->description);
						$o_permission->setStatus($o_result->status);

						$this->ar_all->put($o_permission);
					}
				}
			}

			return true;
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Retorna uma permissão através de um ID
	* 
	* @param void
	* @return boolean
	*/
	public function getById() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "SELECT p.* FROM permission p WHERE p.id = ? LIMIT 1";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					if ($o_result = $o_stmt->fetch(PDO::FETCH_OBJ)) { 
						$this->getRow($o_result);
						return true;
					}
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Grava uma nova Permissão
	* 
	* @param void
	* @return integer
	*/
	public function save() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "INSERT INTO permission p (p.name, p.description) VALUES(?, ?, 1)";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getName());
			$o_stmt->bindValue(2, $this->getDescription());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					$in_id = $o_stmt->lastInsertId();
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $in_id;
	}


	/**
	* Atualiza os dados de uma Permissão
	* 
	* @param void
	* @return boolean
	*/
	public function update() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';

		$st_sql = "UPDATE permission p SET p.name = ?, p.description = ? WHERE p.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getName());
			$o_stmt->bindValue(2, $this->getDescription());
			$o_stmt->bindValue(3, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}

	/**
	* Habilita uma Permissão
	* 
	* @param void
	* @return boolean
	*/
	public function enable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE permission p SET p.status = 1 WHERE p.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}


	/**
	* Desabilita uma Permissão
	* 
	* @param void
	* @return boolean
	*/
	public function disable() {
		$st_caseSensitive = ($this->isCaseSensitive()) ? 'BINARY' : '';
		$in_id = 0;

		$st_sql = "UPDATE permission p SET p.status = 0 WHERE p.id = ?";

		try {
			$o_stmt = $this->o_db->prepare($st_sql);
			$o_stmt->bindValue(1, $this->getId());

			if ($o_stmt->execute()) {
				if ($o_stmt->rowCount() > 0) { 
					return true;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}
}
?>